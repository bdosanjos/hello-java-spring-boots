package com.curso.api;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.curso.api.entidades.Curso;
import com.curso.api.services.CursoServices;

@SpringBootTest
class CursoApiApplicationTests {
	
	@Autowired
	private CursoServices cursoService;

	@Test
	void contextLoads() {
		
		List<Curso> cursos = cursoService.getCurso();
		System.out.println("CURSOS: ");
		System.out.println(cursos);
	}

}
