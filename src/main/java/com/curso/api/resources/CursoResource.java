package com.curso.api.resources;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.curso.api.entidades.Curso;
import com.curso.api.entidades.dto.CursoDTO;
import com.curso.api.entidades.mapper.CursoMappear;
import com.curso.api.services.CursoServices;




//Resource retorna as coisas para as páginas ou os locais de acesso.
@RestController
@RequestMapping("/cursos")
public class CursoResource {
	
	@Autowired
	private CursoServices cursoService;
	
	@Autowired
	private CursoMappear mapper;
	
	@GetMapping
	public ResponseEntity<List<Curso>>getCursos() {
		List<Curso> lista = cursoService.getCurso();
		return ResponseEntity.ok(lista);
	}
	

	@GetMapping("/NumId/{id}")
	public ResponseEntity<Curso> findCurso(@PathVariable Integer id){
		Curso curso = cursoService.findById(id);
		return ResponseEntity.ok().body(curso);
		
	}
	
	@GetMapping("/nome")
	public ResponseEntity<List<Curso>> findCursoByNome(@RequestParam String nomeCurso){
		List<Curso> curso = cursoService.findByNome(nomeCurso);
		return ResponseEntity.ok().body(curso);
		
	}
	
	@PostMapping("/salva")
	public ResponseEntity<Curso> saveCurso(@RequestBody CursoDTO dto) throws URISyntaxException{
		
		
		Curso novoCurso = cursoService.save(mapper.mapCursoDTotoCurso(dto));
		
		return ResponseEntity.created(new URI("/cursos/salva/"+novoCurso.getId())).body(novoCurso);
		
		
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Curso> saveCurso(@RequestBody CursoDTO dto, @PathVariable Integer id) throws URISyntaxException{
		
		
		Curso novo = mapper.mapCursoDTotoCurso(dto);
		novo.setId(id);
		cursoService.update(novo);
		
		return ResponseEntity.noContent().build();
		
		
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Curso> saveCurso( @PathVariable Integer id) throws URISyntaxException{
		
		

		cursoService.deleteById(id);
		
		return ResponseEntity.noContent().build();
		
		
	}
	
	


}
