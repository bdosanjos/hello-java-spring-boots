package com.curso.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.curso.api.entidades.Alunos;

public interface AlunosReposutory extends JpaRepository<Alunos, Integer> {

}
