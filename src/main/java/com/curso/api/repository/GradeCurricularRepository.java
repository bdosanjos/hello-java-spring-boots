package com.curso.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.curso.api.entidades.GradeCurricular;

public interface GradeCurricularRepository extends JpaRepository<GradeCurricular, Integer> {

}
