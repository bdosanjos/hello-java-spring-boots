package com.curso.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.curso.api.entidades.Materia;

public interface MateriaReposutory extends JpaRepository<Materia, Integer> {

}
