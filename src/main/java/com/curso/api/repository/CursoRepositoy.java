package com.curso.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.curso.api.entidades.Curso;

public interface CursoRepositoy extends JpaRepository<Curso, Integer> {
	
	//Padraão find + Nome da entidade + by + campo da entidade
	List<Curso> findCursoByNome(String nome);
	
	
	//Busca like com os % % no começo e fim
	List<Curso> findCursoByNomeContaining(String valor);
	
	//Busca like flexivel, tem com colocar % % na chamada
	List<Curso> findCursoByNomeLike(String valor);
	//Busca like flexivel, tem com colocar % % na chamada e ignora o case sensitive
	List<Curso> findCursoByNomeLikeIgnoreCase(String valor);



}