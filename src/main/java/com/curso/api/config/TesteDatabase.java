package com.curso.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.curso.api.entidades.Curso;
import com.curso.api.repository.CursoRepositoy;


@Component
@Profile(value = "teste")
public class TesteDatabase implements CommandLineRunner{

	
	@Autowired
	private CursoRepositoy cursoRepositoy;
	
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
		Curso cursoUm = new Curso("graduação em dança","Humanas");
		Curso cursoDois = new Curso("graduação em rage","Da Vida");
		Curso cursoTres = new Curso("graduação em delete","Da Vida");
		Curso curso4 = new Curso("graduação em Adiministração","Exatas");
		Curso curso5 = new Curso("graduação em medicina","Saúde");
		Curso curso6 = new Curso("graduação em farmacia","Saúde");
		Curso curso7 = new Curso("graduação em musica","Humanas");
		
		
		cursoRepositoy.save(cursoUm);
		cursoRepositoy.save(cursoDois);
		cursoRepositoy.save(cursoTres);
		cursoRepositoy.save(curso4);
		cursoRepositoy.save(curso5);
		cursoRepositoy.save(curso6);
		cursoRepositoy.save(curso7);
		
		
	}
}
