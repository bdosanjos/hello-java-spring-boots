package com.curso.api.entidades;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "grade")
public class GradeCurricular implements Serializable{

	
	private static final long serialVersionUID = 1L;

	//Colunas
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private  Integer id;  
	
	
	@Column( nullable = false)
	private String objetivo;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(
			name = "aluno_id",
			referencedColumnName = "id"
			)
	private Alunos aluno;
	
	@ManyToMany(mappedBy = "grades")
	private Set<Materia> materias = new HashSet<>();
	
	public GradeCurricular() {
		// TODO Auto-generated constructor stub
	}

	public GradeCurricular(String objetivo, Alunos aluno) {
		super();
		this.objetivo = objetivo;
		this.aluno = aluno;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public Alunos getAluno() {
		return aluno;
	}

	public void setAluno(Alunos aluno) {
		this.aluno = aluno;
	}

	public Set<Materia> getMaterias() {
		return materias;
	}

	public void setMaterias(Set<Materia> materias) {
		this.materias = materias;
	}
	
	
}
