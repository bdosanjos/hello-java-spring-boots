package com.curso.api.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;


@Entity
@Table(name = "curso")
public class Curso implements Serializable{

	
	private static final long serialVersionUID = 1L;
//Colunas
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private  Integer id;  
	
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@Column(name = "area", nullable = false)
	private String area;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@CreationTimestamp
	@Column(name = "data_de_criacao")
	private LocalDateTime dataDeCriacao;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@UpdateTimestamp
	@Column(name = "data_de_atualizacao")
	private LocalDateTime dataDeAtualizcao;
	
	
	@NotNull
	private String usuario;
	
	
	//Faz com que ele não seja criado no banco de dados
	@Transient
	private BigDecimal valorDoCurso;
	

	//Mapeando a relação com a entidade Alunos
	@OneToMany(mappedBy = "curso")
	private List<Alunos> alunos = new ArrayList<>();
	
	
//Métodos
	
	@PostPersist
	private void aposPersistirDados() {
		this.nome = this.nome + " POST";
	}
	
	@PrePersist
	private void antesDePersistir() {
		
		//Colocar o nome do usuario em um sistema real
		this .usuario = "Admin";
	}
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Curso() {
		// TODO Auto-generated constructor stub
	}

	public Curso(String nome, String area) {
		super();
		this.nome = nome;
		this.area = area;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDateTime getDataDeCriacao() {
		return dataDeCriacao;
	}

	public void setDataDeCriacao(LocalDateTime dataDeCriacao) {
		this.dataDeCriacao = dataDeCriacao;
	}

	public LocalDateTime getDataDeAtualização() {
		return dataDeAtualizcao;
	}

	public void setDataDeAtualização(LocalDateTime dataDeAtualização) {
		this.dataDeAtualizcao = dataDeAtualização;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public BigDecimal getValorDoCurso() {
		return valorDoCurso;
	}

	public void setValorDoCurso(BigDecimal valorDoCurso) {
		this.valorDoCurso = valorDoCurso;
	}

	public List<Alunos> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Alunos> alunos) {
		this.alunos = alunos;
	}


	
	
	
}
