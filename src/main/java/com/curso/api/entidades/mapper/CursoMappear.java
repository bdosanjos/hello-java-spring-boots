package com.curso.api.entidades.mapper;

import org.springframework.stereotype.Service;

import com.curso.api.entidades.Curso;
import com.curso.api.entidades.dto.CursoDTO;


@Service
public class CursoMappear {
	
	public Curso mapCursoDTotoCurso (CursoDTO dto) {
		Curso curso =new Curso(dto.getNome(),dto.getArea());
		return curso;
		
	}

}
