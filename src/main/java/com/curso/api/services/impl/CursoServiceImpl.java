package com.curso.api.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.api.entidades.Curso;
import com.curso.api.repository.CursoRepositoy;
import com.curso.api.services.CursoServices;



@Service
public class CursoServiceImpl implements CursoServices {
	
	@Autowired
	private CursoRepositoy cursoRepository;

	@Override
	public List<Curso> getCurso() {
		// TODO Auto-generated method stub
		return cursoRepository.findAll();
	}

	@Override
	public Curso save(Curso curso) {
		// TODO Auto-generated method stub
		return cursoRepository.save(curso);
	}

	@Override
	public Curso findById(Integer id) {
		
		Optional<Curso> curso = cursoRepository.findById(id);
		
		return curso.orElse(null);
	}

	@Override
	public List<Curso> findByNome(String nome) {
		
		return cursoRepository.findCursoByNomeContaining(nome);
	}

	@Override
	public void update(Curso novo) {
		
		Curso atual = this.findById(novo.getId());
		//Pode ser gerado em um metodo separado
		atual.setNome(novo.getNome());
		atual.setArea(novo.getArea());
		
		cursoRepository.save(atual);
		
	}

	@Override
	public void deleteById(Integer id) {
		cursoRepository.deleteById(id);
		
	}

}
