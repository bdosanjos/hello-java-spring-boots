package com.curso.api.services;

import java.util.List;

import com.curso.api.entidades.Curso;


public interface CursoServices {
	
	public List<Curso> getCurso();

	public Curso save(Curso curso);

	public Curso findById(Integer id);

	public List<Curso> findByNome(String nome);

	public void update(Curso novo);
	
	public void deleteById(Integer id);

}
