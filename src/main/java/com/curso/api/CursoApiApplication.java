package com.curso.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.curso.api.entidades.Curso;
import com.curso.api.repository.CursoRepositoy;

@SpringBootApplication
public class CursoApiApplication  {


	public static void main(String[] args) {
		SpringApplication.run(CursoApiApplication.class, args);
	}


}
